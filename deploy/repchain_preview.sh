#!/bin/sh

app_name="RepChain.jar"
sys_tag="121000005l35120456.node1"

#::=== logger
logger_name="logback"

#::=== config
config_base="conf/"
config_app="system.conf"
config_log="logback.xml"

#::=== arguments

#::=== execute
#-Dconfig.resourse=$config_base/$config_app
source /etc/profile
nohup java -Xmx4096m -Djava.rmi.server.hostname=192.168.2.131 -Dcom.sun.management.jmxremote.port=8888 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -D$logger_name.configurationFile=$config_base/$config_log -jar $app_name $sys_tag >/dev/null 2>log &
