package repchain.performance;

import java.io.File;
import java.io.IOException;

/**
 * @author zyf
 */
public class PerformanceTestV1 {

    // 删除tranRes目录下除ReadMe.md的所有文件
    static {
        File tranRes = new File("record/tranRes/");
        if (tranRes.isDirectory()) {
            for (File file : tranRes.listFiles(pathName -> !pathName.getName().endsWith("md"))) {
                file.delete();
            }
        }
    }

    public static void main(String[] args) {

        HostAndFilePath[] hfps = new HostAndFilePath[5];

        hfps[0] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8081").setFilePath("record/tran/RepChainApi-jdk13-10000-node1.txt").build();
        hfps[1] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8081").setFilePath("record/tran/RepChainApi-jdk13-10000-node2.txt").build();
        hfps[2] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8085").setFilePath("record/tran/RepChainApi-jdk13-10000-node3.txt").build();
        hfps[3] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8087").setFilePath("record/tran/RepChainApi-jdk13-10000-node4.txt").build();
        hfps[4] = HostAndFilePath.newBuilder().setHost("192.168.2.69:8089").setFilePath("record/tran/RepChainApi-jdk13-10000-node5.txt").build();

        int nodeHostNum = 2;

        // 也可分别设置
        for (int i = 0; i < nodeHostNum; i++) {
            int finalI = i;
            new Thread(() -> {
                try {
                    new Performancer(hfps[finalI]).start();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }


    static class Performancer {

        HostAndFilePath hostAndFilePath;

        Performancer(HostAndFilePath hostAndFilePath) {
            this.hostAndFilePath = hostAndFilePath;
        }

        /**
         * 使用默认的参数
         *
         * @throws IOException
         * @throws InterruptedException
         */
        void start() throws IOException, InterruptedException {
            start(5, 10, 5, 5, false);
        }

        /**
         * 往多个节点同时压测时候，在开启计算时延的情况下，一定要将并发设置为一样，如果不开启计算时延，可以并发不一样
         *
         * @param thinkTime   请求间隔时间, mills
         * @param concurrency 并发请求数
         * @param sampleTime  采样间隔时间，用来计算tps与ctps
         * @param delayStart  启动延时, seconds
         * @param caculDelay  是否计算交易时延
         * @throws IOException
         * @throws InterruptedException
         */
        void start(int thinkTime, int concurrency, int sampleTime, int delayStart, boolean caculDelay) throws IOException, InterruptedException {

            Performance per = Performance.newBuilder()
                    .setHost(hostAndFilePath.getHost())
                    .setThinkTime(thinkTime) // 每个请求（同步）间隔时间mills
                    .setConcurrency(concurrency) // 并发
                    .setSampleTime(sampleTime) // 采样时间 seconds
                    .setDelayStart(delayStart) // seconds
                    .setTranFilePath(hostAndFilePath.getFilePath())
                    .build();

            per.init();
            per.loadTranData();
            per.startPostTran();
            per.caculateTpsAndCtps();
            if (caculDelay)
                per.caculateDelay();

        }
    }
}
