package repchain.performance;


import com.google.common.util.concurrent.AtomicDouble;
import com.rcjava.client.ChainInfoClient;
import com.rcjava.protos.Peer;
import org.apache.commons.lang3.concurrent.ConcurrentUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.common.Statistics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;


/**
 * @author zyf
 */
public class PerformanceTestSG {

    public static void main(String[] args) throws Exception {

//        List<String> hostList = Arrays.asList("82.156.215.98:8081", "82.156.215.98:8082","82.156.215.98:8083","82.156.215.98:8084");
        List<String> hostList = Arrays.asList(args);
        Performancer performancer = new Performancer(hostList, 5, 1, 20000, 5, 20);
        performancer.init();
        performancer.start();
        performancer.caculateTpsAndCtps();

    }

    private static class Performancer {

        private List<String> hostList;
        // 并发发送交易的总线程 == 节点数 * 并发数
        private CountDownLatch preDownLatch;
        // 加载交易的总线程 == 节点数 * 并发数
        private CountDownLatch tranloadDownLatch;
        // 启动时延  seconds
        private int delayStart = 5;
        // 并发数目
        private int concurrency = 3;
        // 每个线程的交易数
        private int tranNum = 10000;
        // 请求间隔时间 millseconds
        private int thinkTime = 3;
        // 采样时间 seconds, 用来计算tps与ctps
        private int sampleTime = 5;
        private PerformanceV3[] performanceV3s;

        private long position = 2L;

        private Logger logger = LoggerFactory.getLogger(Performancer.class);

        public Performancer(List<String> hostList, int delayStart, int concurrency, int tranNum, int thinkTime, int sampleTime) {
            this.hostList = hostList;
            this.delayStart = delayStart;
            this.concurrency = concurrency;
            this.tranNum = tranNum;
            this.thinkTime = thinkTime;
            this.sampleTime = sampleTime;
            this.preDownLatch = new CountDownLatch(hostList.size() * concurrency);
            this.tranloadDownLatch = new CountDownLatch(hostList.size() * concurrency);
            this.performanceV3s = new PerformanceV3[hostList.size()];
            this.position = new ChainInfoClient(hostList.get(0)).getChainInfo().getHeight() + 1;
        }

        /**
         * 初始化，加载数据
         *
         * @throws InterruptedException
         */
        void init() throws InterruptedException {
            for (int i = 0; i < hostList.size(); i++) {
                int finalI = i;
                String host = hostList.get(i);
                performanceV3s[i] = PerformanceV3.newBuilder()
                        .setHost(host)
                        .setThinkTime(thinkTime) // 每个请求间隔时间mills
                        .setConcurrency(concurrency) // 并发
                        .setTranNum(tranNum) // 每个线程发送交易数
                        .setDelayStart(delayStart) // seconds
                        .setPreDownLatch(preDownLatch)
                        .setTranloadDownLatch(tranloadDownLatch)
                        .build();
                performanceV3s[i].init();
                new Thread(() -> performanceV3s[finalI].loadTranData(), "LoadData---" + finalI + "---" + host).start();
                logger.info("节点{}加载数据...", host);
            }
            // 等待所有线程加载完数据
            tranloadDownLatch.await();
            logger.info("所有节点数据加载完成....");
        }

        /**
         * 开始向各个节点发送交易
         */
        void start() {
            for (int i = 0; i < hostList.size(); i++) {
                int finalI = i;
                new Thread(() -> {
                    try {
                        performanceV3s[finalI].startPostTran();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }, "Performance---" + hostList.get(finalI)).start();
                logger.info("节点{}开始发送数据", hostList.get(i));
            }
        }

        /**
         * 计算实时的tps、ctps，以及平均tps、ctps
         */
        public void caculateTpsAndCtps() throws InterruptedException {

            // 这里修改了一下，应该是hostList
            List<String> consensusList = Arrays.asList("192.168.0.85:8081", "192.168.0.93:8081", "192.168.0.46:8081", "192.168.0.75:8081");
            Statistics statistics = new Statistics(preDownLatch);
            statistics.caculateTpsAndCtps(consensusList, PerformanceV3.getPostReqTranNum(), delayStart, sampleTime);

            for (PerformanceV3 performanceV3 : performanceV3s) {
                performanceV3.getMultiThreadPool().shutdownNow();
            }

            // 间隔 30s，让未出的交易都出完
            TimeUnit.SECONDS.sleep(30);
            // 打印输出delay
            computeDelay(consensusList);

        }


        /**
         * 计算时延
         *
         * @param hostList 向这些节点查询chainInfo
         */
        private void computeDelay(List<String> hostList) {
            // 初始化用来查询链信息的chainInfoClient
            int nodeNums = hostList.size();
            ChainInfoClient[] infoClients = new ChainInfoClient[nodeNums];
            for (int i = 0; i < nodeNums; i++) {
                infoClients[i] = new ChainInfoClient(hostList.get(i));
            }
            ExecutorService delayService = Executors.newFixedThreadPool(hostList.size());
            ConcurrentHashMap<String, MutablePair<Long, Long>> txidPostTimeMap = new ConcurrentHashMap<>();
            // blockTranMap = new TreeMap<>((h1, h2) -> Long.compare(h1, h2));
            SortedMap<Long, Integer> blockTranNums = Collections.synchronizedSortedMap(new TreeMap<>(Comparator.comparingLong(height -> height)));
            AtomicDouble minDelay = new AtomicDouble(Double.MAX_VALUE);
            AtomicDouble maxDelay = new AtomicDouble(Double.MIN_VALUE);
            AtomicDouble sumDelay = new AtomicDouble(0);
            AtomicLong errCount = new AtomicLong(0);
            AtomicLong tranNum = new AtomicLong(0);
            for (PerformanceV3 performanceV3 : performanceV3s) {
                txidPostTimeMap.putAll(performanceV3.getTxidPostTime());
            }
            logger.info("txidPostTimeMap size is: {}", txidPostTimeMap.size());
            CountDownLatch downLatch = new CountDownLatch(infoClients.length);
            long blockHeight = infoClients[0].getChainInfo().getHeight();
            long position = this.position;
            long quotient = (blockHeight - position + 1) / infoClients.length;
            long remainder = (blockHeight - position + 1) % infoClients.length;
            for (int i = 0; i < infoClients.length; i++) {
                int finalI = i;
                long start = position;
                long end = i < infoClients.length - 1 ? position + quotient : position + quotient + remainder;
                logger.info("host: {}, start: {}, end: {}", infoClients[i].getHost(), start, end);
                delayService.execute(() -> {
                    for (long j = start; j < end; j++) {
                        Peer.Block block = infoClients[finalI].getBlockByHeight(j);
                        if (block != null) {
                            long seconds = block.getEndorsements(0).getTmLocal().getSeconds();
                            long millis = block.getEndorsements(0).getTmLocal().getNanos() / 1000000;
                            long createTimeUtc = (seconds * 1000 + millis) - 8 * 3600 * 1000;
                            // logger.info("block height is : {}", block.getHeight());
                            block.getTransactionsList().forEach(tran -> {
                                txidPostTimeMap.computeIfPresent(tran.getId(), (key, value) -> {
                                    value.setLeft(createTimeUtc - value.left);
                                    return value;
                                });
                                if (txidPostTimeMap.containsKey(tran.getId())) {
                                    double delay = BigDecimal.valueOf(txidPostTimeMap.get(tran.getId()).left).doubleValue() / 1000;
                                    minDelay.set(Math.min(minDelay.get(), delay));
                                    maxDelay.set(Math.max(maxDelay.get(), delay));
                                    sumDelay.addAndGet(delay);
                                    tranNum.incrementAndGet();
                                } else {
                                    errCount.incrementAndGet();
                                }
                            });
                            // 这里不用担心线程不安全，因为block高度是不冲突的，即key是不会冲突的
                            blockTranNums.put(block.getHeight(), block.getTransactionsCount());
                        }
                    }
                    downLatch.countDown();
                });
                position += quotient;
            }
            try {
                downLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 不属于本轮压测的交易数, 调试用
            logger.trace("error count number is : {}", errCount.get());
            // 处理的交易数
            logger.info("tran number is : {}", tranNum.get());
            // 峰值delay
            logger.info("max delay is : {}", maxDelay.get());
            // 最小delay
            logger.info("min delay is : {}", minDelay.get());
            // 平均delay
            logger.info("average delay is : {}", sumDelay.get() / tranNum.get());
            try {
                // 输出交易在区块中的分布
                computeBlockTran(blockTranNums);
                // api 请求响应时间
                computeApiTimeDistribution(txidPostTimeMap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * 输出交易在区块中的分布
         *
         * @param blockTranMap
         * @throws IOException
         */
        private void computeBlockTran(SortedMap<Long, Integer> blockTranMap) throws IOException {
            BufferedWriter writer = Files.newBufferedWriter(new File("./blockTranNums.txt").toPath(), StandardCharsets.UTF_8);
            blockTranMap.forEach((height, tranNums) -> {
                try {
                    writer.write(String.join(",", String.valueOf(height), String.valueOf(tranNums)));
                    writer.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.flush();
            writer.close();
        }

        /**
         * 接口响应耗时
         */
        private void computeApiTimeDistribution(ConcurrentHashMap<String, MutablePair<Long, Long>> txidPostTimeMap) throws IOException {
            BufferedWriter writer = Files.newBufferedWriter(new File("./apiResponseTime.txt").toPath(), StandardCharsets.UTF_8);
            TreeMap<Long, Integer> apiTimeMap = new TreeMap<>(Comparator.comparingLong(time -> time));
            // API 请求耗时记录
            txidPostTimeMap.values().forEach(pair -> {
                apiTimeMap.computeIfPresent(pair.right, (key, value) -> value + 1);
                apiTimeMap.putIfAbsent(pair.right, 1);
            });
            apiTimeMap.forEach((time, count) -> {
                try {
                    writer.write(time + "," + count);
                    writer.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.flush();
            writer.close();
        }

    }

}
