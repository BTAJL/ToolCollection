package repchain.performance;

import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.rcjava.client.ChainInfoClient;
import com.rcjava.protos.Peer;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.common.ClientUtil;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author zyf
 */
public class Performance {

    // 负载host
    private String host;
    // 请求间隔时间 millseconds
    private int thinkTime;
    // 并发数目
    private int concurrency;
    // 启动时延  seconds
    private int delayStart;
    // 采样时间 seconds
    private int sampleTime;
    // 构造好的交易文件
    private String tranFilePath;
    // 分配给每个线程进行处理
    private ArrayList<List<String>> jobListArray;
    // 用来计算tps
    private AtomicLong postReqTranNum = new AtomicLong(0L);
    // logging
    private Logger logger = LoggerFactory.getLogger(getClass());

    private ClientUtil clientUtil = new ClientUtil(true);

    private ThreadFactory namedThreadFactory;
    private ExecutorService multiThreadPool;

    // 并发发送交易的线程
    private CountDownLatch preDownLatch;
    // 后处理计算时延的线程
    private CountDownLatch suffDownLatch;

    private Performance(Builder builder) {
        host = builder.host;
        thinkTime = builder.thinkTime;
        concurrency = builder.concurrency;
        delayStart = builder.delayStart;
        sampleTime = builder.sampleTime;
        tranFilePath = builder.tranFilePath;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * @throws IOException
     */
    public void init() throws IOException {

        this.preDownLatch = new CountDownLatch(concurrency);
        this.suffDownLatch = new CountDownLatch(concurrency);

        this.namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("performance-pool-%d").build();
        this.multiThreadPool = new ThreadPoolExecutor(concurrency, 32, 0L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

        for (int i = 0; i < concurrency; i++) {
            String filePathName = String.format("record/tranRes/tranRes_%s", i);
            File tranRes = new File(filePathName);
            if (!tranRes.exists()) {
                tranRes.createNewFile();
            }
        }
    }

    /**
     * 装载交易数据，为每个线程装一个list
     */
    public boolean loadTranData() throws IOException {
        List<String> tranList = FileUtils.readLines(new File(tranFilePath), StandardCharsets.UTF_8);
        int tranNum = tranList.size();
        int quotient = tranNum / concurrency;
        int remainder = tranNum % concurrency;
        int start = 0;
        int end = quotient;
        jobListArray = new ArrayList<>();
        for (int i = 0; i < concurrency; i++) {
            jobListArray.add(tranList.subList(start, end));
            start += quotient;
            end += (i != (concurrency - 1)) ? (quotient) : (remainder + quotient);
        }
        return true;
    }

    /**
     * 启动，每个线程都遍历自己的list进行发送交易
     */
    public void startPostTran() throws InterruptedException {

        // 延迟若干秒启动
        TimeUnit.SECONDS.sleep(delayStart);
//        Performance postClient = this;
        String postUrl = String.format("http://%s/transaction/postTranByString", host);
        // 开始发送交易
        for (int i = 0; i < concurrency; i++) {
            int finalI = i;
            multiThreadPool.execute(() -> {
                        File tranResFile = new File(String.format("record/tranRes/tranRes_%s", finalI));
                        jobListArray.get(finalI).forEach(tran -> {
                            long realTime = System.currentTimeMillis();
                            JSONObject jsonObject = clientUtil.post(postUrl, tran);
                            if (jsonObject.containsKey("err")) {
                                logger.info("Transaction’s txid is {}, err is {}", jsonObject.getString("txid"), jsonObject.getString("err"));
                            }
                            String tranId = jsonObject.getString("txid");
                            String res = tranId + "," + realTime + System.lineSeparator();
                            postReqTranNum.incrementAndGet();
                            try {
                                // 保存结果数据，txid,timeStamp
                                FileUtils.writeStringToFile(tranResFile, res, StandardCharsets.UTF_8, true);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            try {
                                thinkTime = thinkTime < 0 ? 0 : thinkTime;
                                TimeUnit.MILLISECONDS.sleep(thinkTime);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        });
                        logger.info("tranList_{}_size_{}", finalI, jobListArray.get(finalI).size());
                        preDownLatch.countDown();
                    }
            );
        }
    }

    /**
     * 计算实时的tps、ctps，以及平均tps、ctps
     */
    public void caculateTpsAndCtps() throws InterruptedException {

        // 峰值Tps与峰值Ctps
        AtomicLong maxTps = new AtomicLong(0L);
        AtomicLong maxCtps = new AtomicLong(0L);

        List tpsList = new ArrayList<Long>();
        AtomicLong startReqTranNums = new AtomicLong(0L);
        ScheduledExecutorService tpsService = Executors.newSingleThreadScheduledExecutor();

        // 计算实时tps
        tpsService.scheduleWithFixedDelay(() -> {
            // TPS
            long realReqTranNums = postReqTranNum.get();
            System.err.println(String.format("当前已发送交易数 : %s", realReqTranNums));
            long realTps = (realReqTranNums - startReqTranNums.get()) / sampleTime;
            logger.info("real Tps is : {}", realTps);
            startReqTranNums.set(realReqTranNums);
            tpsList.add(realTps);
            maxTps.getAndUpdate(preTps -> Math.max(preTps, realTps));
        }, delayStart, sampleTime, TimeUnit.SECONDS);

        ChainInfoClient infoClient = new ChainInfoClient(host);

        List ctpsList = new ArrayList<Long>();
        AtomicLong startConfirmedTranNums = new AtomicLong(0L);
        startConfirmedTranNums.set(infoClient.getChainInfo().getTotalTransactions());
        ScheduledExecutorService ctpsService = Executors.newSingleThreadScheduledExecutor();

        // 计算实时ctps
        ctpsService.scheduleWithFixedDelay(() -> {
            // CTPS
            Peer.BlockchainInfo chainInfo = infoClient.getChainInfo();
            long realTranNums = chainInfo.getTotalTransactions();
            System.err.println(String.format("当前链上交易数 : %s", realTranNums));
            long realCtps = (realTranNums - startConfirmedTranNums.get()) / sampleTime;
            logger.info("real Ctps is : {}", realCtps);
            startConfirmedTranNums.set(realTranNums);
            ctpsList.add(realCtps);
            maxCtps.getAndUpdate(preTps -> Math.max(preTps, realCtps));
        }, delayStart, sampleTime, TimeUnit.SECONDS);

        // 挂起等待提交交易的线程组结束
        preDownLatch.await();
        multiThreadPool.shutdown();

        // 结束计算ctps的线程
        while (true) {
            TimeUnit.SECONDS.sleep(sampleTime);
            if (preDownLatch.getCount() == 0) {
                // 任务中断，中断这个schedule线程
                tpsService.shutdown();
                ctpsService.shutdown();
                // 平均tps
                logger.info("average Tps is : {}", Arrays.stream(tpsList.toArray()).mapToLong(elem -> (Long) elem).sum() / tpsList.size());
                // 平均ctps
                logger.info("average Ctps is : {}", Arrays.stream(ctpsList.toArray()).mapToLong(elem -> (Long) elem).sum() / ctpsList.size());
                // 峰值Tps
                logger.info("peak Tps is : {}", maxTps.get());
                // 峰值Ctps
                logger.info("peak Ctps is : {}", maxCtps.get());
                break;
            }
        }
    }

    /**
     * 计算最终时延
     */
    public void caculateDelay() throws IOException, InterruptedException {
        TimeUnit.SECONDS.sleep(sampleTime);
        logger.info("正在计算中...请等待");
        while (true) {
            // 加载缓存时，容易超时，不用管，在下面的等待30s即可
            clientUtil.get(String.format("http://%s/chaininfo/loadBlockInfoToCache", host));
            logger.info("加载缓存时，容易超时，不用管，等待60s即可...");
            TimeUnit.SECONDS.sleep(60);
            JSONObject loadRes = clientUtil.get(String.format("http://%s/chaininfo/IsLoadBlockInfoToCache", host));
            if (loadRes != null && loadRes.getString("isfinish").equals("1")) {
                break;
            }
        }

        long sumTranNum = 0L;
        jobListArray = new ArrayList<>();
        for (int i = 0; i < concurrency; i++) {
            List tranReslist = FileUtils.readLines(new File(String.format("record/tranRes/tranRes_%s", i)), StandardCharsets.UTF_8);
            jobListArray.add(tranReslist);
            sumTranNum += tranReslist.size();
        }

        AtomicLong minDelay = new AtomicLong(0L);
        AtomicLong maxDelay = new AtomicLong(0L);
        AtomicLong sumDelay = new AtomicLong(0L);
        AtomicLong queryFailed = new AtomicLong(0L);

        String postUrl = String.format("http://%s/block/blocktimeoftran", host);
        for (int i = 0; i < concurrency; i++) {
            int finalI = i;
            multiThreadPool.execute(() -> {
                jobListArray.get(finalI).forEach(resString -> {
                    String[] resArray = resString.split(",");
                    String txid = resArray[0];
                    long timeStamp = Long.valueOf(resArray[1]);
//                    String postDta = JSON.toJSONString(String.format("{\"txid\":\"%s\"}", txid));
                    JSONObject jsonObject = clientUtil.post(postUrl, String.format("{\"txid\":\"%s\"}", txid));
                    if (jsonObject == null) {
                        throw new RuntimeException("RepChain未loadCache，请核查");
                    }
                    String blockTimeStampStr = jsonObject.getString("createTimeUtc");
                    if (blockTimeStampStr.equals("")) {
                        // 记录的交易，此刻还未上链，在链上未查询到
                        queryFailed.incrementAndGet();
                        return;
                    }
                    logger.debug("txid: {}, timeStamp: {}", txid, blockTimeStampStr);
                    long blockTimeStamp = Long.valueOf(blockTimeStampStr);
                    long delay = Math.max(blockTimeStamp - timeStamp, 0);
                    minDelay.getAndUpdate(preElem -> Math.min(preElem, delay));
                    maxDelay.getAndUpdate(preElem -> Math.max(preElem, delay));
                    sumDelay.getAndAdd(delay > 0 ? delay : 0);
                });
                suffDownLatch.countDown();
            });
        }
        suffDownLatch.await();
        logger.info("min delay is : {} millSeconds", minDelay.get());
        logger.info("max delay is : {} millSeconds", maxDelay.get());
        logger.info("average delay is : {} millSeconds", sumDelay.doubleValue() / sumTranNum);
        logger.info("query txid failed at this moment, num is : {}", queryFailed.get());
        multiThreadPool.shutdown();
    }

    public static final class Builder {
        private String host;
        private int thinkTime;
        private int concurrency;
        private int delayStart;
        private int sampleTime;
        private String tranFilePath;

        private Builder() {
        }

        public Builder setHost(String host) {
            this.host = host;
            return this;
        }

        public Builder setThinkTime(int thinkTime) {
            this.thinkTime = thinkTime;
            return this;
        }

        public Builder setConcurrency(int concurrency) {
            this.concurrency = concurrency;
            return this;
        }

        public Builder setDelayStart(int delayStart) {
            this.delayStart = delayStart;
            return this;
        }

        public Builder setSampleTime(int sampleTime) {
            this.sampleTime = sampleTime;
            return this;
        }

        public Builder setTranFilePath(String tranFilePath) {
            this.tranFilePath = tranFilePath;
            return this;
        }

        public Performance build() {
            return new Performance(this);
        }
    }
}
