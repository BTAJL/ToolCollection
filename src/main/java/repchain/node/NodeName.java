package repchain.node;

/**
 * 枚举变量，节点的creditCode，name，tagName
 *
 * @author zyf
 */
public enum NodeName {

    // Node名字
    NODE1("121000005l35120456", "node1", "121000005l35120456.node1"), NODE2("12110107bi45jh675g", "node2", "12110107bi45jh675g.node2"),
    NODE3("122000002n00123567", "node3", "122000002n00123567.node3"), NODE4("921000005k36123789", "node4", "921000005k36123789.node4"),
    NODE5("921000006e0012v696", "node5", "921000006e0012v696.node5"), SUPER_ADMIN("951002007l78123233", "super_admin", "951002007l78123233.super_admin");

    private String creditCode;
    private String nodeName;
    private String nodeTag;

    NodeName(String creditCode, String nodeName, String nodeTag) {
        this.creditCode = creditCode;
        this.nodeName = nodeName;
        this.nodeTag = nodeTag;
    }

    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeTag() {
        return nodeTag;
    }

    public void setNodeTag(String nodeTag) {
        this.nodeTag = nodeTag;
    }
}
