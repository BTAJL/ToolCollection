package repchain.node;

import java.util.ArrayList;
import java.util.List;

/**
 * 节点的host地址
 *
 * @author zyf
 */
public class NodeHost {

    private String host;

    public static String node1Host = "192.168.2.131:8081";
    public static String node2Host = "192.168.2.132:8081";
    public static String node3Host = "192.168.2.133:8081";
    public static String node4Host = "192.168.2.134:8081";
    public static String node5Host = "192.168.2.135:8081";

    public static List<String> hostList = new ArrayList<>();


    static {
        hostList.add(node1Host);
        hostList.add(node2Host);
        hostList.add(node3Host);
        hostList.add(node4Host);
        hostList.add(node5Host);
    }

    public NodeHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
