package repchain.genjks;

import com.rcjava.util.PemUtil;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import repchain.node.NodeTag;

import java.io.*;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.Date;
import java.util.Enumeration;

/**
 * 生成Jks，privateKey，自签名证书
 */
public class GenerateJks {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    // 生成Jks的路径
    private static String jksDirectory = "newJks/";
    private static String certsDirectory = "newCerts/";
    // 组网的节点数，最多生成16个（class NodeTag 中设置的）
    private static int nodeNum = 16;

    public static void main(String[] args) throws Exception {

        // 创建目录，如果存在了，那么清理干净
        File jksDir = new File(jksDirectory);
        File certsDir = new File(certsDirectory);
        if (!jksDir.exists()) {
            jksDir.mkdirs();
        }
        if (!certsDir.exists()) {
            certsDir.mkdirs();
        }
        FileUtils.cleanDirectory(jksDir);
        FileUtils.cleanDirectory(certsDir);

        // 1. 生成super_admin的Jks
        String super_admin = "super_admin";
        genJksFile(NodeTag.NODE_TAG_MAP.get(super_admin), super_admin, super_admin);

        // 2. 生成组网节点node的Jks
        for (int i = 1; i <= nodeNum; i++) {
            String commonName = "node" + i;
            genJksFile(NodeTag.NODE_TAG_MAP.get(commonName), "123", commonName);
        }

        // 3. 过滤nodeJks与super_adminJks
        File[] nodeFiles = jksDir.listFiles((file, name) -> name.endsWith(".jks") && (name.contains("node") || name.contains("super_admin")));

        // 4. 将nodeJks与super_adminJks里的证书导入并存储到myTrustKeyStore
        String trustJksName = jksDirectory + "mytruststore.jks";
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);

        for (File nodeFile : nodeFiles) {
            KeyStore nodeJks = KeyStore.getInstance(KeyStore.getDefaultType());
            try {
                nodeJks.load(new FileInputStream(nodeFile), "123".toCharArray());
            } catch (IOException ioex) {
                nodeJks.load(new FileInputStream(nodeFile), "super_admin".toCharArray());
            }
            Enumeration enumeration = nodeJks.aliases();
            while (enumeration.hasMoreElements()) {
                String alias = (String) enumeration.nextElement();
                Certificate nodeCert = nodeJks.getCertificate(alias);
                // 将nodeCert保存到trustStore
                trustStore.setCertificateEntry(alias, nodeCert);
                // 输出nodeCert到newCerts目录
                String certName = certsDirectory + alias + ".cer";
                PemUtil.exportToPemFile(new File(certName), nodeCert);
            }
        }
        trustStore.store(new FileOutputStream(new File(trustJksName)), "changeme".toCharArray());

        // TODO 输出共识列表

    }

    /**
     * 生成自签名Jks
     *
     * @param alias      别名
     * @param password   密码
     * @param commonName 证书CN
     * @throws Exception
     */
    public static void genJksFile(String alias, String password, String commonName) throws Exception {

        // 使用secp256r1初始化
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC", "BC");
        keyPairGenerator.initialize(new ECGenParameterSpec("P-256"));

        // 生成keypair
        KeyPair keyPair = keyPairGenerator.genKeyPair();

        // 初始化Jks
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(null, null);

        // 生成self-signed V3Certificate
        X509CertificateHolder x509CertificateHolder = createTrustHolder(keyPair, "SHA256withECDSA", commonName);

        X509Certificate x509Certificate = new JcaX509CertificateConverter().setProvider("BC").getCertificate(x509CertificateHolder);

        keyStore.setKeyEntry(alias, keyPair.getPrivate(), password.toCharArray(), new Certificate[]{x509Certificate});

        String jksName = jksDirectory + alias + ".jks";

        FileOutputStream fileOutputStream = new FileOutputStream(new File(jksName));

        keyStore.store(fileOutputStream, password.toCharArray());

        fileOutputStream.close();
    }

    /**
     * @param keyPair    密钥对
     * @param sigAlg     使用sigAlg来签名证书
     * @param commonName
     * @return
     */
    public static X509CertificateHolder createTrustHolder(KeyPair keyPair, String sigAlg, String commonName) throws OperatorCreationException {

        X500NameBuilder x500NameBuilder = new X500NameBuilder(BCStyle.INSTANCE);
        X500Name x500Name = x500NameBuilder
                .addRDN(BCStyle.CN, commonName)
                .addRDN(BCStyle.OU, "iscas")
                .addRDN(BCStyle.O, "RepChain")
                .build();

        X509v3CertificateBuilder certificateBuilder = new JcaX509v3CertificateBuilder(
                x500Name,
                BigInteger.valueOf(System.currentTimeMillis()),
                CalculateDate(0),
                CalculateDate(24 * 365 * 5),
                x500Name,
                keyPair.getPublic()
        );

        ContentSigner contentSigner = new JcaContentSignerBuilder(sigAlg).setProvider("BC").build(keyPair.getPrivate());

        return certificateBuilder.build(contentSigner);
    }

    /**
     * @param hoursInFuture
     * @return
     */
    private static Date CalculateDate(int hoursInFuture) {
        long secondsNow = System.currentTimeMillis() / 1000;
        return new Date((secondsNow + (hoursInFuture * 60 * 60)) * 1000);
    }
}
