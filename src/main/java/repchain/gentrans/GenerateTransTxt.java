package repchain.gentrans;

import com.alibaba.fastjson.JSON;

import com.rcjava.protos.Peer;
import com.rcjava.tran.TranCreator;
import com.rcjava.util.CertUtil;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repchain.model.Transfer;

import java.io.File;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

/**
 * 使用多线程来构造签名交易字符串，并保存到指定的txt文件中
 *
 * @author zyf
 */
public class GenerateTransTxt {

    private static Logger logger = LoggerFactory.getLogger("out");

    private static Transfer transfer = new Transfer("121000005l35120456", "12110107bi45jh675g", 1);

    private static Peer.CertId certId = Peer.CertId.newBuilder().setCreditCode("121000005l35120456").setCertName("node1").build(); // 签名ID
    //这个是给转账交易示范用的，此ID需要与repchain合约部署的一致
    private static Peer.ChaincodeId contractAssetsId = Peer.ChaincodeId.newBuilder().setChaincodeName("ContractAssetsTPL").setVersion(1).build();

    private static PrivateKey privateKey = CertUtil.genX509CertPrivateKey(
            new File("jks/121000005l35120456.node1.jks"),
            "123",
            "121000005l35120456.node1").getPrivateKey();

    private static TranCreator tranCreator = TranCreator.newBuilder()
            .setPrivateKey(privateKey)
            .setSignAlgorithm("sha256withecdsa")
            .build();

    public static void main(String[] args) throws IOException, InterruptedException {

        int tranNum = 50;
//        List<String> list = new ArrayList<>();
        List<String> synArrayList = Collections.synchronizedList(new ArrayList<>());

        int threadNum = 6;
        CountDownLatch downLatch = new CountDownLatch(threadNum);
        int quotient = tranNum / threadNum;
        int remainder = tranNum % threadNum;

        for (int i = 0; i < threadNum; i++) {
            int finalI = i;
            new Thread(() -> {
                for (int j = 0; j < ((finalI != (threadNum - 1)) ? quotient : (remainder + quotient)); j++) {
                    String tranId = UUID.randomUUID().toString().replace("-", "");
                    Peer.Transaction tran = tranCreator.createInvokeTran(tranId, certId, contractAssetsId, "transfer", JSON.toJSONString(transfer));
                    String tranHex = Hex.encodeHexString(tran.toByteArray());
                    synArrayList.add(JSON.toJSONString(tranHex));
                    System.out.println("synArrayList size is : " + synArrayList.size());
                    logger.info("tranId: {}", tranId);
                }
                downLatch.countDown();
            }, String.format("thread-name: thread-%s", i)).start();
        }
        downLatch.await();
        System.out.println("synArrayList size is : " + synArrayList.size());
        FileUtils.writeLines(new File(String.format("record/tran/RepChainApi-jdk13-%s-win.txt", tranNum)), "UTF-8", synArrayList);
    }
}
