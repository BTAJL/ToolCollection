package repchain.common;

import com.google.common.util.concurrent.AtomicDouble;
import com.rcjava.client.ChainInfoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

import static java.nio.file.StandardOpenOption.*;

/**
 * 统计tps与ctps
 *
 * @author zyf
 */
public class Statistics {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private ScheduledExecutorService tpsService = Executors.newSingleThreadScheduledExecutor();
    private ScheduledExecutorService ctpsService = Executors.newSingleThreadScheduledExecutor();

    private ArrayList<Long> tpsList = new ArrayList<>();
    private ArrayList<Long> ctpsList = new ArrayList<>();

    private CountDownLatch preDownLatch = new CountDownLatch(0);

    public Statistics() {
    }

    public Statistics(CountDownLatch preDownLatch) {
        this.preDownLatch = preDownLatch;
    }

    /**
     * 计算实时的tps、ctps，以及平均tps、ctps
     */
    public void caculateTpsAndCtps(List<String> hostList, LongAdder postReqTranNum, int delayStart, int sampleTime) throws InterruptedException {

        // 峰值Tps与峰值Ctps
        AtomicDouble maxTps = new AtomicDouble(0);
        AtomicDouble maxCtps = new AtomicDouble(0);

        AtomicLong startReqTranNums = new AtomicLong(0L);

        // 计算实时tps
        tpsService.scheduleWithFixedDelay(() -> {
            // TPS
            long realReqTranNums = postReqTranNum.longValue();
            System.err.println(String.format("当前已发送交易数 : %s", realReqTranNums));
            long realTps = (realReqTranNums - startReqTranNums.get()) / sampleTime;
            logger.info("real Tps is : {}", realTps);
            startReqTranNums.set(realReqTranNums);
            tpsList.add(realTps);
            maxTps.set(Math.max(maxTps.get(), realTps));
        }, delayStart, sampleTime, TimeUnit.SECONDS);

        // 初始化用来查询链信息的chainInfoClient
        int nodeNums = hostList.size();
        ChainInfoClient[] infoClients = new ChainInfoClient[nodeNums];
        for (int i = 0; i < nodeNums; i++) {
            infoClients[i] = new ChainInfoClient(hostList.get(i));
        }

        Long[] confirmedTranArray = new Long[nodeNums];
        AtomicLong startConfirmedTranNums = new AtomicLong(infoClients[0].getChainInfo().getTotalTransactions());

        // 计算实时ctps
        ctpsService.scheduleWithFixedDelay(() -> {
            // CTPS, 向所有节点去查询
            for (int i = 0; i < nodeNums; i++) {
                try {
                    confirmedTranArray[i] = infoClients[i].getChainInfo().getTotalTransactions();
                } catch (Exception ex) {
                    logger.info("getChainInfo err, the errMsg is {}", ex.getMessage(), ex);
                }
            }
            // 排序，去掉最小值，去掉最大值，剩余取平均
            Arrays.sort(confirmedTranArray);
            logger.info("各个节点查询到的上链交易数：{}", Arrays.toString(confirmedTranArray));
            long realConfirmedTranNums;
            if (confirmedTranArray.length == 1) {
                realConfirmedTranNums = sumLongArray(confirmedTranArray, 0, 0);
            } else if (confirmedTranArray.length == 2) {
                realConfirmedTranNums = sumLongArray(confirmedTranArray, 0, 1) / 2;
            } else {
                realConfirmedTranNums = sumLongArray(confirmedTranArray, 1, confirmedTranArray.length - 2) / (confirmedTranArray.length - 2);
            }
            System.err.println(String.format("当前链上交易数 : %s", realConfirmedTranNums));
            Long realCtps = (realConfirmedTranNums - startConfirmedTranNums.get()) / sampleTime;
            logger.info("real Ctps is : {}", realCtps);
            startConfirmedTranNums.set(realConfirmedTranNums);
            ctpsList.add(realCtps);
            maxCtps.set(Math.max(maxCtps.get(), realCtps));
        }, delayStart, sampleTime, TimeUnit.SECONDS);

        // ctrl+c
        Runtime.getRuntime().addShutdownHook(new Thread(() -> tpsAndCtpsResult(tpsList, ctpsList, maxTps.get(), maxCtps.get())));

        if (preDownLatch.getCount() != 0) {
            // 挂起等待提交交易的线程组结束
            preDownLatch.await();

            // 任务中断，中断这个schedule线程
            tpsService.shutdown();
            ctpsService.shutdown();

            // 结束计算ctps的线程
            TimeUnit.SECONDS.sleep(2 * sampleTime);

            // 打印输出 tps/ctps
            tpsAndCtpsResult(tpsList, ctpsList, maxTps.get(), maxCtps.get());
        }

    }

    /**
     * 将tps与ctps的采样值写入到txt中，方便后续进行excel进行绘图
     *
     * @param list tpsList或ctpsList
     * @param file tpsFile或ctpsFile
     */
    private void writeListToFile(List<Long> list, File file) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(file.toPath(), StandardCharsets.UTF_8, CREATE, WRITE, TRUNCATE_EXISTING);
        list.forEach(
                value -> {
                    try {
                        writer.write(String.valueOf(value));
                        writer.newLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        );
        writer.flush();
        writer.close();
    }

    /**
     * 打印结果
     *
     * @param tpsOriginalList  tps原始列表
     * @param ctpsOriginalList ctps原始列表
     * @param maxTps           峰值Tps
     * @param maxCtps          峰值Ctps
     */
    private void tpsAndCtpsResult(List<Long> tpsOriginalList, List<Long> ctpsOriginalList, double maxTps, double maxCtps) {
        List<Long> tpsList = new ArrayList<>(tpsOriginalList);
        List<Long> ctpsList = new ArrayList<>(ctpsOriginalList);
        File tpsFile = new File("./tps.txt");
        File ctpsFile = new File("./ctps.txt");
        try {
            // 将tps写入到文件
            writeListToFile(tpsList, tpsFile);
            // 将ctps写入文件
            writeListToFile(ctpsList, ctpsFile);
        } catch (IOException ioEx) {
            logger.error(ioEx.getMessage(), ioEx);
        }
        // 对列表排序
        Collections.sort(tpsList);
        Collections.sort(ctpsList);
        // 去掉一个最大值，去掉一个最小值，然后求取平均值
        // 平均tps
        logger.info("average Tps is : {}", sumLongList(tpsList, 1, tpsList.size() - 2) / (tpsList.size() - 2));
        // 平均ctps
        logger.info("average Ctps is : {}", sumLongList(ctpsList, 1, ctpsList.size() - 2) / (ctpsList.size() - 2));
        // 峰值Tps
        logger.info("peak Tps is : {}", maxTps);
        // 峰值Ctps
        logger.info("peak Ctps is : {}", maxCtps);
    }

    /**
     * 对数组区间进行求和
     *
     * @param array
     * @param start
     * @param end
     * @return
     */
    private static Long sumLongArray(Long[] array, int start, int end) {
        long sumRes = 0;
        for (int i = start; i <= end; i++) {
            sumRes += array[i];
        }
        return sumRes;
    }

    /**
     * 对list的区间进行求和
     *
     * @param list
     * @param start
     * @param end
     * @return
     */
    private static Long sumLongList(List<Long> list, int start, int end) {
        long sumRes = 0;
        for (int i = start; i <= end; i++) {
            sumRes += list.get(i);
        }
        return sumRes;
    }

    public ScheduledExecutorService getTpsService() {
        return tpsService;
    }

    public ScheduledExecutorService getCtpsService() {
        return ctpsService;
    }

    public ArrayList<Long> getTpsList() {
        return tpsList;
    }

    public ArrayList<Long> getCtpsList() {
        return ctpsList;
    }
}