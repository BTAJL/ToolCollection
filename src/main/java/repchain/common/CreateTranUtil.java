package repchain.common;


import com.alibaba.fastjson.JSON;
import com.rcjava.protos.Peer;
import com.rcjava.tran.TranCreator;
import com.rcjava.util.CertUtil;
import repchain.model.Transfer;
import repchain.node.NodeName;

import java.io.File;
import java.security.PrivateKey;
import java.util.Random;
import java.util.UUID;

/**
 * @author zyf
 */
public class CreateTranUtil {


    private static NodeName[] nodeNames;
    private static PrivateKey[] privateKeys;
    private static TranCreator[] tranCreators;

    private static Random random = new Random();
    private static Peer.ChaincodeId contractAssetsId = Peer.ChaincodeId.newBuilder().setChaincodeName("ContractAssetsTPL").setVersion(1).build();

    static {

        nodeNames = NodeName.values();
        privateKeys = new PrivateKey[5];
        tranCreators = new TranCreator[5];

        for (int i = 0; i < 5; i++) {
            privateKeys[i] = CertUtil.genX509CertPrivateKey(new File(String.format("jks/%s.jks", nodeNames[i].getNodeTag())), "123", nodeNames[i].getNodeTag()).getPrivateKey();
            tranCreators[i] = TranCreator.newBuilder().setPrivateKey(privateKeys[i]).setSignAlgorithm("SHA256withECDSA").build();
        }

    }


    /**
     * 构造转账的invoke交易
     *
     * @return
     */
    public static Peer.Transaction createTran() {

        int fromIndex = random.nextInt(5);
        int toIndex = random.nextInt(5);

        NodeName from = nodeNames[fromIndex];
        NodeName to = nodeNames[toIndex];

        Transfer transfer = new Transfer(from.getCreditCode(), to.getCreditCode(), 1);
        Peer.CertId certId = Peer.CertId.newBuilder().setCreditCode(from.getCreditCode()).setCertName(from.getNodeName()).build();

        String tranId = UUID.randomUUID().toString().replace("-", "");
        return tranCreators[fromIndex].createInvokeTran(tranId, certId, contractAssetsId, "transfer", JSON.toJSONString(transfer));

    }

    /**
     * 构造重复交易ID的交易
     *
     * @param tranId
     * @return
     */
    public static Peer.Transaction createDuplicateTran(String tranId) {

        int fromIndex = random.nextInt(5);
        int toIndex = random.nextInt(5);

        NodeName from = nodeNames[fromIndex];
        NodeName to = nodeNames[toIndex];

        Transfer transfer = new Transfer(from.getCreditCode(), to.getCreditCode(), 1);
        Peer.CertId certId = Peer.CertId.newBuilder().setCreditCode(from.getCreditCode()).setCertName(from.getNodeName()).build();

        return tranCreators[fromIndex].createInvokeTran(tranId, certId, contractAssetsId, "transfer", JSON.toJSONString(transfer));

    }

    /**
     * 构造错误的交易
     *
     * @return
     */
    public static Peer.Transaction createErrorTran() {

        while (true) {

            int fromIndex = random.nextInt(5);
            int toIndex = random.nextInt(5);

            if (fromIndex != toIndex) {

                NodeName from = nodeNames[fromIndex];
                NodeName to = nodeNames[toIndex];

                Transfer transfer = new Transfer(from.getCreditCode(), to.getCreditCode(),1);
                Peer.CertId certId = Peer.CertId.newBuilder().setCreditCode(to.getCreditCode()).setCertName(to.getNodeName()).build();

                String tranId = UUID.randomUUID().toString().replace("-", "");
                return tranCreators[toIndex].createInvokeTran(tranId, certId, contractAssetsId, "transfer", JSON.toJSONString(transfer));

            }

        }

    }
}
