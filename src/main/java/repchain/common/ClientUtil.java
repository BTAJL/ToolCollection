package repchain.common;

import com.alibaba.fastjson.JSONObject;
import com.rcjava.client.RClient;
import com.rcjava.client.TranPostClient;
import com.rcjava.client.async.RAsyncClient;
import com.rcjava.client.async.TranPostAsyncClient;
import com.rcjava.protos.Peer;
import org.apache.http.HttpResponse;

import java.util.concurrent.Future;

import static java.util.Objects.requireNonNull;
import static repchain.common.ClientUtil.AsyncClient.resolveResponseFuture;

/**
 * @author zyf
 */
public class ClientUtil {

    private String host = null;
    private boolean syncMode = true;
    private TranPostClient postClient = null;
    private TranPostAsyncClient postAsyncClient = null;

    private SyncClient syncClient = new SyncClient();
    private AsyncClient asyncClient = new AsyncClient();

    public ClientUtil() {
    }

    public ClientUtil(String host) {
        this(host, true);
    }

    public ClientUtil(boolean syncMode) {
        this.syncMode = syncMode;
    }

    public ClientUtil(String host, boolean syncMode) {
        this.host = host;
        this.syncMode = syncMode;
        this.postClient = new TranPostClient(host);
        this.postAsyncClient = new TranPostAsyncClient(host);
    }

    /**
     * @param url
     * @param json
     * @return
     */
    public JSONObject post(String url, String json) {
        if (syncMode) {
            return syncClient.post(url, json);
        } else {
            return asyncClient.post(url, json);
        }
    }

    /**
     * @param url
     * @return
     */
    public JSONObject get(String url) {
        if (syncMode) {
            return syncClient.get(url);
        } else {
            return asyncClient.get(url);
        }
    }

    /**
     * @param tran
     */
    public JSONObject post(Peer.Transaction tran) {
        assert host != null;
        if (syncMode) {
            return postClient.postSignedTran(tran);
        } else {
            return resolveResponseFuture(postAsyncClient.postSignedTran(tran));
        }
    }

    /**
     * @param tranHexString
     */
    public JSONObject post(String tranHexString) {
        requireNonNull(host);
        if (syncMode) {
            return postClient.postSignedTran(tranHexString);
        } else {
            return resolveResponseFuture(postAsyncClient.postSignedTran(tranHexString));
        }
    }


    class SyncClient extends RClient {

        public JSONObject post(String url, String json) {
            JSONObject jsonObject = super.postJString(url, json);
            return jsonObject;
        }

        public JSONObject get(String url) {
            JSONObject jsonObject = super.getJObject(url);
            return jsonObject;
        }

    }

    static class AsyncClient extends RAsyncClient {

        public JSONObject post(String url, String json) {
            Future<HttpResponse> responseFuture = super.postJString(url, json);
            JSONObject jsonObject = resolveHttpResponseFuture(responseFuture);
            return jsonObject;
        }

        public JSONObject get(String url) {
            Future<HttpResponse> responseFuture = super.getResponse(url);
            JSONObject jsonObject = resolveHttpResponseFuture(responseFuture);
            return jsonObject;
        }

        public static JSONObject resolveResponseFuture(Future<HttpResponse> responseFuture) {
            return RAsyncClient.resolveHttpResponseFuture(responseFuture);
        }
    }
}
