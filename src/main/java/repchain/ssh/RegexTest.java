package repchain.ssh;

import com.google.common.io.Files;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zyf
 */
public class RegexTest {

    public static void main(String[] args) {

        String host = "hostname = 192.168.2.199";
        String repHost = host.replaceAll("\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}", "192.168.2.131");
        System.out.println(repHost);

        String host1 = "canonical.hostname = 192.168.2.199";

        String alias = "alias = \"121000005l35120456.node1\"";
        String repAlias = alias.replaceAll("\"[\\w\\.\\w]*\"", "121000005l35120456.node2");
//        String repAlias = alias.replaceAll("\"[a-z|0-9]{18}\\.[a-z|0-9]{4,}\"", "121000005l35120456.node2");
//        String repAlias = alias.replaceAll("\"[a-z|0-9|\\.]*\"", "121000005l35120456.node2");
//        String repAlias = alias.replaceAll("\"[\\w\\.]*\"", "121000005l35120456.node2");
        System.out.println(repAlias);

        String str = "   hostname = 192.168.2.199";
        String pattern = "\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}";
        System.out.println(str.replaceAll("(\\s+hostname.+)", "192.168.2.13"));
        System.out.println(Pattern.compile("\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}").matcher(str).replaceAll("192.168.2.13"));

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(str);
        System.out.println(m.lookingAt());

        String sysTag = "sys_tag=\"121000005l35120456.node1\"";
        String repTag = sysTag.replaceAll("^sys_tag", "121000005l35120456.node2");
        System.out.println(repTag);

    }

}
