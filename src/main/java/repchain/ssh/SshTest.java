package repchain.ssh;

import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;

/**
 * @author zyf
 */
public class SshTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) throws InterruptedException, JSchException, IOException, SftpException {

        SshTest sshTest = new SshTest();

//        sshTest.testJschClient();

        sshTest.testSftpClient();

    }

    public void testJschClient() throws JSchException, InterruptedException {

        JSch jsch = new JSch();

        Session session = jsch.getSession("root", "192.168.2.131", 22);

//        session.setPassword("iscas");

        //set auth info interactively
        session.setUserInfo(new RcUserInfo());

        session.connect();

        ChannelExec ec = (ChannelExec) session.openChannel("exec");
        ec.setCommand("ifconfig");
        ec.setInputStream(null);
        ec.setErrStream(System.err);
        ec.setOutputStream(System.out);
        ec.connect();

        while (!ec.isClosed())
            Thread.sleep(1000);

        ec.disconnect();
        session.disconnect();
    }

    public void testSftpClient() throws JSchException, SftpException, IOException {

        JSch jsch = new JSch();

        Session session = jsch.getSession("root", "192.168.2.131", 22);

        session.setConfig("StrictHostKeyChecking", "no");
        session.setPassword("iscas");

        session.connect();

        com.jcraft.jsch.ChannelSftp sftpChannel = (com.jcraft.jsch.ChannelSftp) session.openChannel("sftp");
        sftpChannel.connect();

        Path src = Paths.get("src.txt");
        Files.deleteIfExists(src);
        Files.createFile(src);
        Files.write(src, "adsfasdfs".getBytes());

        @SuppressWarnings("unchecked")
        Vector<ChannelSftp.LsEntry> vector = sftpChannel.ls(".");
        for (ChannelSftp.LsEntry entry : vector)
            System.out.println(entry.getFilename() + "    " + entry.getAttrs().getSize());

        sftpChannel.cd("test");
        sftpChannel.put("src.txt", "dst.txt");
        sftpChannel.get("dst.txt", "dst1.txt");
        sftpChannel.rm("dst.txt");

        sftpChannel.disconnect();
        session.disconnect();
        System.out.println("done");
    }
}
