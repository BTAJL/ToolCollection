package repchain.ssh;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author zyf
 */
public class DistributeConfig {

    private static Logger logger = LoggerFactory.getLogger(DistributeConfig.class);

    private static String repDir = "/home/iscas1";

    public static void main(String[] args) throws IOException {

        List<String> configList = ConfigUtil.readSystemConfig();
        List<String> startShList = ConfigUtil.readStartShConfig();
        List<String> hostList = ConfigUtil.readHost();
        List<String> nodeList = ConfigUtil.readNodeName();

        System.out.println("deploy/system.conf中请自行修改seed-nodes，以及其他各个节点的统一项，以及在指定目录打包了repchain.zip，请确认");
        logger.info("deploy/system.conf中请自行修改seed-nodes，以及其他各个节点的统一项，以及在指定目录打包了repchain.zip，请确认");

        // 检测到换行或者回车就继续往下走
        while (true) {
            if (System.in.read() == 10 || System.in.read() == 13) {
                break;
            }
        }

        System.out.println("准备工作：1、在服务器端创建具体的目录；2、安装软件unzip，用来解压缩repchain.zip");
        logger.info("准备工作：1、在服务器端创建具体的目录；2、安装软件unzip，用来解压缩repchain.zip");

        // 准备工作：1、在服务器端创建具体的目录；2、安装软件unzip，用来解压缩repchain.zip
        hostList.forEach(hostIp -> {
            RcServerInfo rcServerInfo = new RcServerInfo(hostIp, 22, "root", "iscas");
            try {
                CommandUtil.execCmd(rcServerInfo, String.format("pwd && ls && mkdir %s && yum install -y unzip", repDir));
            } catch (JSchException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        System.out.println("准备工作：3、上传repchain.zip（即部署目录的压缩包，其中包含RepChain.jar等文件或目录）");
        logger.info("准备工作：3、上传repchain.zip（即部署目录的压缩包，其中包含RepChain.jar等文件或目录）");

        // 准备工作：3、上传repchain.zip（即部署目录的压缩包，其中包含RepChain.jar等文件或目录）
        hostList.forEach(hostIp -> {
            RcServerInfo rcServerInfo = new RcServerInfo(hostIp, 22, "root", "iscas");
            try {
                ConfigUtil.uploadConfig(rcServerInfo, "D:\\jdk13Performance\\repchain.zip", repDir);
                CommandUtil.execCmd(rcServerInfo, String.format("cd %s && unzip -o repchain.zip", repDir));
            } catch (JSchException e) {
                e.printStackTrace();
            } catch (SftpException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        System.out.println("准备工作：4、修改配置文件与repchain_preview.sh，分发到各个节点上");
        logger.info("准备工作：4、修改配置文件与repchain_preview.sh，分发到各个节点上");

        // 准备工作：4、修改配置文件与repchain_preview.sh，分发到各个节点上
        for (int i = 0; i < hostList.size(); i++) {
            String hostIp = hostList.get(i);
            for (int j = 0; j < configList.size(); j++) {
                String line = configList.get(j);
                // 搜索canonical.hostname的配置项，搜到之后，替换其中的ip地址
                if (Pattern.matches("(\\s+canonical\\.hostname.+)", line)) {
                    // 简单的匹配一下ip，并将其替换为具体的host
                    configList.set(j, line.replaceAll("\\d.+\\d", hostIp));
                }
            }
            String nodeName = nodeList.get(i);
            for (int j = 0; j < startShList.size(); j++) {
                String line = startShList.get(j);
                if (Pattern.matches("^sys_tag.+", line)) {
                    startShList.set(j, line.replaceAll("\"[\\w\\.\\w]*\"", String.format("\"%s\"", nodeName)));
                }
                if (Pattern.matches("nohup.+", line)) {
                    startShList.set(j, line.replaceAll("\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}\\.\\d{0,3}", hostIp));
                }
            }

            // 将修改后的配置以及启动脚本写到新文件中
            try {
                FileUtils.writeLines(new File("deploy/distribute/system.conf"), "utf-8", configList);
                FileUtils.writeLines(new File("deploy/distribute/repchain_preview.sh"), "utf-8", startShList);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // 分发上传system.conf文件与repchain_preview.sh
            RcServerInfo rcServerInfo = new RcServerInfo(hostIp, 22, "root", "iscas");
            try {
                ConfigUtil.uploadConfig(rcServerInfo, "deploy/distribute/system.conf", repDir + "/repchain/conf");
                ConfigUtil.uploadConfig(rcServerInfo, "deploy/distribute/repchain_preview.sh", repDir + "/repchain/");
            } catch (JSchException e) {
                e.printStackTrace();
            } catch (SftpException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Congratulations，everything done");

    }

    /**
     * 启动RepChain，到具体的服务器上执行repchain_preview.sh脚本
     *
     * @param hostIp
     */
    public void startRepChain(String hostIp) {

        RcServerInfo rcServerInfo = new RcServerInfo(hostIp, 22, "root", "iscas");
        try {
            CommandUtil.execCmd(rcServerInfo, String.format("cd %s/repchain && sh repchain_preview.sh", repDir));
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
