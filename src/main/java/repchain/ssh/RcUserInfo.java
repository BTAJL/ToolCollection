package repchain.ssh;

import com.jcraft.jsch.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 相关用户信息
 *
 * @author zyf
 */
public class RcUserInfo implements UserInfo {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private String password = "iscas";

    public RcUserInfo() {
    }

    public RcUserInfo(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public boolean promptYesNo(String str) {
        logger.info("promptYesNo message: {}", str);
        return true;
    }

    public String getPassphrase() {
        return "无提示密码信息";
    }

    public boolean promptPassphrase(String message) {
        logger.info("promptPassphrase message: {} ", message);
        return true;
    }

    public boolean promptPassword(String message) {
        logger.info("promptPassword message: {}", message);
        return true;
    }

    public void showMessage(String message) {
        logger.info("showMessage message: {}", message);
    }
}
